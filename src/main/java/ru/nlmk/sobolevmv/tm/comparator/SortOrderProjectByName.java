package ru.nlmk.sobolevmv.tm.comparator;

import ru.nlmk.sobolevmv.tm.entity.Project;
import java.util.Comparator;

public class SortOrderProjectByName implements Comparator<Project>{

  @Override
  public int compare(Project o1, Project o2) {
    return o1.getName().compareTo(o2.getName());
  }
}
