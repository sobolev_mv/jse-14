package ru.nlmk.sobolevmv.tm.service;

import ru.nlmk.sobolevmv.tm.entity.User;
import ru.nlmk.sobolevmv.tm.enumerated.Role;
import ru.nlmk.sobolevmv.tm.repository.UserRepository;
import ru.nlmk.sobolevmv.tm.util.HashMD5;

import java.util.List;

public class UserService {

  private final UserRepository userRepository;

  private Long userSignInId;

  public UserService(final UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  public UserService(final UserRepository userRepository, Long userSignInId) {
    this.userRepository = userRepository;
    this.userSignInId = userSignInId;
  }

  public User create(final String login, final String userPassword, final String firstName, final String lastName, final String middleName) {
    if (login == null || login.isEmpty()) return null;
    if (userPassword == null || userPassword.isEmpty()) return null;
    if (firstName == null || firstName.isEmpty()) return null;
    if (lastName == null || lastName.isEmpty()) return null;
    if (middleName == null || middleName.isEmpty()) return null;
    return userRepository.create(login, userPassword, firstName, lastName, middleName);
  }

  public User create(final String login, final String userPassword, final String firstName, final String lastName, final String middleName, Role userRole) {
    if (login == null || login.isEmpty()) return null;
    if (userPassword == null || userPassword.isEmpty()) return null;
    if (firstName == null || firstName.isEmpty()) return null;
    if (lastName == null || lastName.isEmpty()) return null;
    if (middleName == null || middleName.isEmpty()) return null;
    return userRepository.create(login, userPassword, firstName, lastName, middleName, userRole);
  }

  public User findByLogin(final String login) {
    if (login == null || login.isEmpty()) return null;
    return userRepository.findByLogin(login);
  }

  public User removeByLogin(final String login) {
    if (login == null || login.isEmpty()) return null;
    return userRepository.removeByLogin(login);
  }

  public User updateByLogin(final String login, final String userPassword, final String firstName, final String lastName, final String middleName){
    if (login == null || login.isEmpty()) return null;
    if (userPassword == null || userPassword.isEmpty()) return null;
    if (firstName == null || firstName.isEmpty()) return null;
    if (lastName == null || lastName.isEmpty()) return null;
    if (middleName == null || middleName.isEmpty()) return null;
    return userRepository.updateByLogin(login, userPassword, firstName, lastName, middleName);
  }

  public User findById(final Long id) {
    if (id == null) return null;
    return userRepository.findById(id);
  }

  public User removeById(final Long id) {
    if (id == null) return null;
    return userRepository.removeById(id);
  }

  public User updateById(final Long id, final String login, final String userPassword, final String firstName, final String lastName, final String middleName) {
    if (id == null) return null;
    if (login == null || login.isEmpty()) return null;
    if (userPassword == null || userPassword.isEmpty()) return null;
    if (firstName == null || firstName.isEmpty()) return null;
    if (lastName == null || lastName.isEmpty()) return null;
    if (middleName == null || middleName.isEmpty()) return null;
    return userRepository.updateById(id, login, userPassword, firstName, lastName, middleName);
  }

  public User userSignIn(String login, String userPassword) {
    User user = userRepository.findByLogin(login);
    if (user == null) return null;
    if (!user.getHashPassword().equals(HashMD5.getHash(userPassword))) return null;
    this.setUserSignInId(user.getId());
    return user;
  }

  public User userChangePassword(String login, String userOldPassword, String userNewPassword) {
    User user = userRepository.findByLogin(login);
    if (user == null) return null;
    if (!user.getHashPassword().equals(HashMD5.getHash(userOldPassword))) return null;
    user.setHashPassword(userNewPassword);
    return user;
  }

  public void userSignOut(){
    this.userSignInId = null;
  }

  public Long getUserSignInId() {
    return userSignInId;
  }

  public void setUserSignInId(Long userSignInId) {
    this.userSignInId = userSignInId;
  }

  public void clear() {
    userRepository.clear();
  }

  public int getSize() {
    return userRepository.getSize();
  }

  public List<User> findAll() {
    return userRepository.findAll();
  }
}
