package ru.nlmk.sobolevmv.tm.repository;

import ru.nlmk.sobolevmv.tm.entity.Project;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class ProjectRepository {

  private List<Project> projects = new ArrayList<>();

  public Project create(final String name){
    final Project project = new Project(name);
    projects.add(project);
    return project;
  }

  public Project create(final String name, final String description) {
    final Project project = create(name);
    project.setDescription(description);
    return project;
  }

  public Project create(final String name, final String description, Long userId) {
    final Project project = create(name, description);
    project.setUserId(userId);
    return project;
  }

  public Project update(final Long id, final String name, final String description) {
    final Project project = findById(id);
    if (project == null) return null;
    project.setId(id);
    project.setName(name);
    project.setDescription(description);
    return project;
  }

  public void clear(){
    projects.clear();
  }

  public Project findByIndex(int index) {
    if(index < 0 || index > projects.size() - 1) return null;
    return projects.get(index);
  }

  public Project findByName(final String name) {
    if (name == null || name.isEmpty()) return null;
    for (final Project project: projects) {
      if (project.getName().equals(name)) return project;
    }
    return null;
  }

  public Project findById(final Long id) {
    if (id == null) return null;
    for (final Project project: projects) {
      if (project.getId().equals(id)) return project;
    }
    return null;
  }

  public Project removeById(final Long id) {
    final Project project = findById(id);
    if (project == null) return null;
    projects.remove(project);
    return project;
  }

  public Project removeByName(final String name) {
    final Project project = findByName(name);
    if (project == null) return null;
    projects.remove(project);
    return project;
  }

  public Project removeByIndex(final int index) {
    final Project project = findByIndex(index);
    if (project == null) return null;
    projects.remove(project);
    return project;
  }

  public int getSize() {
    return projects.size();
  }

  public List<Project> findAll(Long userId)  {
    if (userId == null) return Collections.emptyList();
    List<Project> lstTmp = new ArrayList<>();
    for(Project project : projects) {
      if (Objects.equals(project.getUserId(), userId)) lstTmp.add(project);
    }
    return lstTmp;
  }
}

